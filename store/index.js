import YAML from 'yaml'
import * as d3 from 'd3'

// const { CI_PAGES_URL } = process.env
// const base = (CI_PAGES_URL && new URL(CI_PAGES_URL).pathname) || ''
const base = '/map-web'

export const state = () => ({
  config: {},
  data: []
})

export const mutations = {
  setConfig (state, payload) {
    state.config = payload
  },

  setData (state, payload) {
    state.data = payload
  }
}

export const actions = {
  async fetchConfig ({ commit }) {
    // Get the yml config file
    const response = await fetch(`${base}/config.yml`)
    const rawText = await response.text()
    const config = YAML.parse(rawText)

    commit('setConfig', config)
  },

  async fetchData ({ dispatch, commit, state }) {
    await dispatch('fetchConfig')

    // Obtengo el csv de datos
    const data = await d3.csv(state.config.dataUrl)

    commit('setData', data)
  }
}
