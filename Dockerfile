FROM node:13.8-buster

RUN apt update

WORKDIR /usr/app

# Install npm deps
COPY package.json yarn.lock ./
RUN yarn install

EXPOSE 3000
