/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    extend: {
      colors: {
        background: {
          'navbar': 'var(--background-navbar)',
          'footer': 'var(--background-footer)',
          'body': 'var(--background-body)'
        }
      },
      textColor: {
        'on-navbar': 'var(--text-on-navbar)',
        'on-footer': 'var(--text-on-footer)',
        'on-home': 'var(--text-on-home)',
        'on-body': 'var(--text-on-body)'
      },
      gridTemplateColumns: {
        'navbar-left': 'minmax(100px, 230px) auto'
      }
    },
    boxShadow: {
      'default': '0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06)',
      'md': ' 0 4px 6px -1px rgba(0, 0, 0, .1), 0 2px 4px -1px rgba(0, 0, 0, .06)',
      'lg': ' 0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05)',
      'xl': ' 0 20px 25px -5px rgba(0, 0, 0, .1), 0 10px 10px -5px rgba(0, 0, 0, .04)',
      '2xl': '0 25px 50px -12px rgba(0, 0, 0, .25)',

      'navbar-t': '0px 5px 5px rgba(0, 0, 0, 0.6)',
      'navbar-l': '5px 0px 5px rgba(0, 0, 0, 0.6)'
    }
  },
  variants: {},
  plugins: []
}
